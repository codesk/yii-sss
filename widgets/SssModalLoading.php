<?php

class SssModalLoading extends CWidget {

    public $assetUrl;
    public $header;

    public function init() {
        parent::init();
        $this->assetUrl = Yii::app()->sss->getAssetsUrl();
        $this->header = isset($this->header) ? $this->header : 'Please wait...';
    }

    public function run() {
        parent::run();
        $this->render('modalLoading', array(
            'header' => $this->header,
        ));
    }

}

?>
