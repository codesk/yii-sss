<div class="sss-widget">
    <?php
    $this->beginWidget('bootstrap.widgets.TbModal', array(
        'id' => $this->id . '-modal-loading',
        'options' => array(
            'backdrop' => 'static',
            'keyboard' => true,
        ),
    ));
    ?>
    <div class="modal-header">
        <h4><?php echo CHtml::encode($header); ?></h4>
    </div>
    <div class="modal-body">
        <div class="center">
            <?php echo CHtml::image($this->assetUrl . '/images/ajax-load.gif'); ?>
        </div>
    </div>
    <div class="modal-footer">
    </div>
    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    function SssModalLoadingShow() {
        $("#<?php echo $this->id; ?>-modal-loading").modal('show');
    }
    function SssModalLoadingHide() {
        $("#<?php echo $this->id; ?>-modal-loading").modal('hide');
    }
</script>