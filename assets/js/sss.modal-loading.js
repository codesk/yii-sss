(function($) {
    var SssModalLoading = function(div, options) {
        this.options = $.extend({}, $.fn.editableform.defaults, options);
        this.$div = $(div);
        if (!this.options.scope) {
            this.options.scope = this;
        }
    };
    SssModalLoading.prototype = {
        constructor: SssModalLoading,
        show: function() {
            $(".sss-loading-modal").modal('show');
        },
        hide: function() {
            $(".sss-loading-modal").modal('hide');
        },
    }
}(window.jQuery));