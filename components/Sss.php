<?php

class Sss extends CApplicationComponent {

    private $_assetsUrl;

    public function init() {
        parent::init();
        if (Yii::getPathOfAlias('sss') === false) {
            Yii::setPathOfAlias('sss', realpath(dirname(__FILE__) . '/..'));
        }
    }

    public function getAssetsUrl() {
        if (isset($this->_assetsUrl)) {
            return $this->_assetsUrl;
        } else {
            return $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('sss.assets'), false, -1, YII_DEBUG);
        }
    }

}

?>
